package com.example.practica_sem_12.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.practica_sem_12.entities.Anime;

import java.util.List;

@Dao
public interface AnimeDAO {
    @Query("SELECT * FROM Animes")
    List<Anime> getAll();

    @Insert
    void create(Anime anime);

    @Update
    void update(Anime anime);
}

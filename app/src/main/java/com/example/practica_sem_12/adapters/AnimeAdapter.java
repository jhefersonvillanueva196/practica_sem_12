package com.example.practica_sem_12.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.practica_sem_12.DetalleAnime;
import com.example.practica_sem_12.R;
import com.example.practica_sem_12.entities.Anime;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AnimeAdapter extends RecyclerView.Adapter<AnimeAdapter.AnimeViewHolder>{

    List<Anime> animes;
    public AnimeAdapter(List<Anime> animes) {
        this.animes = animes;
    }

    @NonNull
    @Override
    public AnimeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_item_anime, parent, false);
        return new AnimeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimeViewHolder vh, int position) {

        View itemView = vh.itemView;

        Anime anime = animes.get(position);
        ImageView image = itemView.findViewById(R.id.image);
        TextView tvNombre = itemView.findViewById(R.id.tvNombre);
        TextView tvDescripcion = itemView.findViewById(R.id.tvDescripcion);

        Picasso.get().load(anime.url).into(image);
        tvNombre.setText("Titulo: " + anime.nombre);
        tvDescripcion.setText(anime.descripcion);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(itemView.getContext(), DetalleAnime.class);

                String contactJSON = new Gson().toJson(anime);
                intent.putExtra("ANIME", contactJSON);

                itemView.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return animes.size();
    }

    class AnimeViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemClickListener {

        public AnimeViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Anime contact = animes.get(i);
        }
    }
}


package com.example.practica_sem_12.services;

import com.example.practica_sem_12.entities.Anime;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AnimeService {

    @GET("animes")
    Call<List<Anime>> getAnimes();

    @POST("animes")
    Call<Anime> create(@Body Anime pokemon);

    @PUT("animes/{id}")
    Call<Anime> update(@Path("id") int id, @Body Anime anime);

}

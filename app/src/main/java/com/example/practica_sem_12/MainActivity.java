package com.example.practica_sem_12;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.practica_sem_12.dao.AnimeDAO;
import com.example.practica_sem_12.database.AppDatabase;
import com.example.practica_sem_12.entities.Anime;
import com.example.practica_sem_12.factories.RetrofitFactory;
import com.example.practica_sem_12.services.AnimeService;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    List<Anime> animesRemoto = new ArrayList<>();
    List<Anime> animesLocal = new ArrayList<>();
    AppDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button crear = findViewById(R.id.Crear);
        Button sincronizar = findViewById(R.id.Sincronizar);
        Button mostrar = findViewById(R.id.Mostrar);

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),FormAnime.class);
                startActivity(intent);
            }
        });

        sincronizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db = AppDatabase.getDatabase(getApplicationContext());

                AnimeDAO dao = db.animeDao();
                animesLocal = dao.getAll();

                Retrofit retrofit = RetrofitFactory.build();
                AnimeService service = retrofit.create(AnimeService.class);

                Call<List<Anime>> call = service.getAnimes();

                call.enqueue(new Callback<List<Anime>>() {
                    @Override
                    public void onResponse(Call<List<Anime>> call, Response<List<Anime>> response) {
                        if(!response.isSuccessful()) {
                            Log.e("Incorrecta", "Error de aplicación");
                        } else {
                            Log.i("Correcta", "Respuesta Correcta");
                            animesRemoto = response.body();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Anime>> call, Throwable t) {
                        Log.e("Error", "Respuesta erronea");
                    }
                });

                Log.e("Correcta", new Gson().toJson(animesRemoto));
                Log.e("Correcta", new Gson().toJson(animesLocal));
                for (int i = 0; i < animesRemoto.size(); i++) {
                    if(animesLocal.size() - 1 >= i){
                        if(animesRemoto.get(i).id == animesLocal.get(i).id){
                            AnimeDAO daoUpdate = db.animeDao();
                            daoUpdate.update(animesRemoto.get(i));
                        }else{
                            AnimeDAO daoInsert = db.animeDao();
                            daoInsert.create(animesRemoto.get(i));
                        }
                    }else{
                        AnimeDAO daoInsert = db.animeDao();
                        daoInsert.create(animesRemoto.get(i));
                    }
                }
            }
        });

        mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ListaAnimes.class);
                startActivity(intent);
            }
        });
    }
}
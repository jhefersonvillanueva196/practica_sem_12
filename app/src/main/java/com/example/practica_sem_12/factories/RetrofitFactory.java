package com.example.practica_sem_12.factories;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFactory {
    public static Retrofit build() {

       return new Retrofit.Builder()
                .baseUrl("https://62863682f0e8f0bb7c126992.mockapi.io/api/PS9/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
}

package com.example.practica_sem_12;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.practica_sem_12.adapters.AnimeAdapter;
import com.example.practica_sem_12.entities.Anime;
import com.example.practica_sem_12.factories.RetrofitFactory;
import com.example.practica_sem_12.services.AnimeService;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetalleAnime extends AppCompatActivity {

    List<Anime> animes = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_anime);

        String contactJson = getIntent().getStringExtra("ANIME");
        Anime anime = new Gson().fromJson(contactJson, Anime.class);

        ImageView image = findViewById(R.id.image);

        TextView tvTitulo = findViewById(R.id.etNombre);
        TextView tvResumen = findViewById(R.id.etDescripcion);
        Button editar = findViewById(R.id.btEditar);

        Picasso.get().load(anime.url).into(image);
        tvTitulo.setText(anime.nombre);
        tvResumen.setText(anime.descripcion);

        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Anime animenew = new Anime();

                animenew.nombre = tvTitulo.getText().toString();
                animenew.descripcion = tvResumen.getText().toString();
                animenew.url = anime.url;

                Retrofit retrofit = RetrofitFactory.build();
                AnimeService service = retrofit.create(AnimeService.class);

                Call<Anime> call = service.update(anime.id, animenew);

                call.enqueue(new Callback<Anime>() {
                    @Override
                    public void onResponse(Call<Anime> call, Response<Anime> response) {
                        if(response.isSuccessful()) {
                            Log.e("VJ0906", "Edicion Correcta");
                        } else {
                            Log.i("VJ0906", "Error de aplicación");
                        }
                    }

                    @Override
                    public void onFailure(Call<Anime> call, Throwable t) {
                        Log.e("VJ0906", "No hubo conectividad con el servicio web");
                    }
                });
            }
        });

    }
}
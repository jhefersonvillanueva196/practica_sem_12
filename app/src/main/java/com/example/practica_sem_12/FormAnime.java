package com.example.practica_sem_12;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.practica_sem_12.entities.Anime;
import com.example.practica_sem_12.factories.RetrofitFactory;
import com.example.practica_sem_12.services.AnimeService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FormAnime extends AppCompatActivity {
    EditText nombre,descripcion,url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_anime);

        nombre = findViewById(R.id.etNombre);
        descripcion = findViewById(R.id.etDescripcion);
        url = findViewById(R.id.etUrl);


        Button Crear = findViewById(R.id.Crear);

        Crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar()==true){
                    Anime anime = new Anime();
                    anime.nombre = nombre.getText().toString();
                    anime.descripcion = descripcion.getText().toString();
                    anime.url = url.getText().toString();

                    Retrofit retrofit = RetrofitFactory.build();
                    AnimeService service = retrofit.create(AnimeService.class);

                    Call<Anime> call = service.create(anime);

                    call.enqueue(new Callback<Anime>() {
                        @Override
                        public void onResponse(Call<Anime> call, Response<Anime> response) {
                            if(response.isSuccessful()){
                                Log.i("Crear", "Se creo correctamente");
                            }else {
                                Log.i("Crear", "No creo correctamente");
                            }
                        }

                        @Override
                        public void onFailure(Call<Anime> call, Throwable t) {

                        }
                    });
                    Toast toast = Toast.makeText(getApplicationContext(), "Se creo correctamente", Toast.LENGTH_LONG);
                    toast.show();
                }else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Todos los campos son obligatorios", Toast.LENGTH_LONG);
                    toast.show();

                }

            }
        });

    }

    public boolean validar(){
        boolean band = true;
        String c1 = nombre.getText().toString();
        String c2 = descripcion.getText().toString();
        String c3 = url.getText().toString();
        if(c1.isEmpty()){
            band = false;
        }
        if(c2.isEmpty()){
            band = false;
        }
        if(c3.isEmpty()){
           band = false;
        }

        return band;
    }
}
package com.example.practica_sem_12.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.practica_sem_12.dao.AnimeDAO;
import com.example.practica_sem_12.entities.Anime;

@Database(entities = {Anime.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract AnimeDAO animeDao();

    public static AppDatabase getDatabase(Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "Animes.db")
                .allowMainThreadQueries()
                .build();
    }
}

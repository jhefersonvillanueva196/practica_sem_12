package com.example.practica_sem_12;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.practica_sem_12.adapters.AnimeAdapter;
import com.example.practica_sem_12.dao.AnimeDAO;
import com.example.practica_sem_12.database.AppDatabase;
import com.example.practica_sem_12.entities.Anime;
import com.example.practica_sem_12.factories.RetrofitFactory;
import com.example.practica_sem_12.services.AnimeService;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;

public class ListaAnimes extends AppCompatActivity {

    AppDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_animes);

        db = AppDatabase.getDatabase(getApplicationContext());

        AnimeDAO dao = db.animeDao();
        List<Anime> animes = dao.getAll();

        AnimeAdapter adapter = new AnimeAdapter(animes);

        RecyclerView rv = findViewById(R.id.rvAnimes);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rv.setHasFixedSize(true);
        rv.setAdapter(adapter);

        Log.i("DB", new Gson().toJson(animes));


    }
}